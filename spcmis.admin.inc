<?php
// $Id$

/*
 * Form for configuring the CMIS endpoint via Site Configuration 
*/
function spcmis_endpoint_form() {
  $form = array();

  $form["cmis_endpoints"] = array(
    '#type' => 'fieldset',
    '#title' => t("Add/Edit SharePoint CMIS endpoint"),
    '#tree' => TRUE,
  );

  $arr = variable_get('cmis_endpoints', NULL);

  $form["cmis_endpoints"]["endpoint"]['url'] = array(
    '#type' => 'textfield',
    '#title' => t("URL"),
    '#default_value' => $arr["endpoint"]["url"],
    '#description' => t("URL of SharePoint site where CMIS is configured."),
    '#required' => TRUE,
  );

  $form["cmis_endpoints"]["endpoint"]['user'] = array(
    '#type' => 'textfield',
    '#title' => t("UserName"),
    '#default_value' => $arr["endpoint"]["user"],
    '#description' => t("UserName for SharePoint CMIS producer"),
    '#required' => TRUE,
  );

  $form["cmis_endpoints"]["endpoint"]['password'] = array(
    '#type' => 'password',
    '#title' => t("Password"),
    '#description' => t("Password for SharePoint CMIS producer"),
    '#required' => TRUE,
  );


  return system_settings_form($form);
}

/*
 * Form for setting the default shared document repository
 * via Site Configuration
*/
function spcmis_lists_form() {
  spcmis_load_includes();
  $form = array();

  try {
    $endpoint = variable_get('cmis_endpoints', NULL);
    if (isset($endpoint)) {

      $client = spcmis_get_client($endpoint);
      $lists = spcmis_get_splist_collection($client);
      //TODO: Figure out a better way to handle this. Removing inbuilt SP libraries.
      $inbuilt_libraries = array('Converted Forms',
        'Customized Reports',
        'Form Templates',
        'List Template Gallery',
        'Master Page Gallery',
        'Reporting Templates',
        'Shared Documents',
        'Site Assets',
        'Site Pages',
        'Solution Gallery',
        'Style Library',
        'Theme Gallery',
        'Web Part Gallery',
        'wfpub',
      );

      $options = array();
      if (isset($lists)) {
        foreach ($lists as $list) {
          if ($list['!BaseType'] == 1 && !in_array($list['!Title'], $inbuilt_libraries)) {
            $options[$list['!ID']] = $list['!Title'];
          }
        }

        $default_library = variable_get('cmis_repositories', NULL);
        $form['cmis_enable'] = array(
          '#type' => 'checkbox',
          '#title' => t('Publish SharePoint library'),
          '#description' => t('Check this checkbox to publish the shared SharePoint Library under community. This library will be available to all users.'),
          '#default_value' => $default_library['default']['repository_enable'],
        );
        $form['cmis_library'] = array(
          '#type' => 'select',
          '#title' => t('Libraries'),
          '#options' => $options,
          '#required' => TRUE,
          '#default_value' => '{' . $default_library['default']['repository_id'] . '}',
          '#description' => t('Select shared library to be published on the site.'),
        );
        $form['save'] = array(
          '#type' => 'submit',
          '#value' => t('Save library'),
        );
      }
      else {
        drupal_set_message(t('The SharePoint CMIS endpoint is not configured properly.'), 'error');
      }
    }
    else {
      drupal_set_message(t('The SharePoint CMIS endpoint is not configured properly.'), 'error');
    }
  }
  catch(Exception$exception) {
    drupal_set_message(t('Can\'t connect to SharePoint CMIS endpoint.'), 'error');
  }
  return $form;
}

/*
 * Custom Submit for saving the endpoint and default shared library settings
*/
function spcmis_lists_form_submit(&$form, &$form_state) {
  try {
    $cmis_library_id = trim($form_state['values']['cmis_library'], "{}");
    $cmis_enable = $form_state['values']['cmis_enable'];

    // Check if variable cmis_repositories already exists in variable table
    $endpoint = variable_get('cmis_endpoints', NULL);
    $cmis_libraries = variable_get('cmis_repositories', NULL);
    if (isset($cmis_libraries)) {
      if (isset($cmis_libraries['default'])) {
        $cmis_libraries['default']['url'] = $endpoint['endpoint']['url'] . '/_vti_bin/cmis/rest/' . $cmis_library_id . '?getRepositoryInfo';
        $cmis_libraries['default']['user'] = $endpoint['endpoint']['user'];
        $cmis_libraries['default']['password'] = $endpoint['endpoint']['password'];
        $cmis_libraries['default']['repository_id'] = $cmis_library_id;
        $cmis_libraries['default']['repository_enable'] = $cmis_enable;
      }
      else {
        $cmis_libraries += array(
          'default' => array(
            'url' => $endpoint['endpoint']['url'] . '/_vti_bin/cmis/rest/' . $cmis_library_id . '?getRepositoryInfo',
            'user' => $endpoint['endpoint']['user'],
            'password' => $endpoint['endpoint']['password'],
            'repository_id' => $cmis_library_id,
            'repository_enable' => $cmis_enable,
          ),
        );
      }
    }
    else {
      $cmis_libraries = array();
      $cmis_libraries += array(
        'default' => array(
          'url' => $endpoint['endpoint']['url'] . '/_vti_bin/cmis/rest/' . $cmis_library_id . '?getRepositoryInfo',
          'user' => $endpoint['endpoint']['user'],
          'password' => $endpoint['endpoint']['password'],
          'repository_id' => $cmis_library_id,
          'repository_enable' => $cmis_enable,
        ),
      );
    }
    variable_set('cmis_repositories', $cmis_libraries);

    if ($cmis_enable == 1) {

      //Delete and insert
      $result = db_query("SELECT * FROM {menu_links} WHERE link_path = '%s' AND plid = %d", array('spcmis/browser', 682));
      while ($link = db_fetch_array($result)) {
        _menu_delete_item($link);
      }

      $item = array(
        'link_path' => 'spcmis/browser',
        'link_title' => 'Shared Documents',
        'menu_name' => 'primary-links',
        'hidden' => 0,
        'plid' => 682,
      );
      // @Todo Error in saving link due to wrong plid...
      menu_link_save($item);
    }
    else {
      $result = db_query("SELECT * FROM {menu_links} WHERE link_path = '%s' AND plid = %d", array('spcmis/browser', 682));
      while ($link = db_fetch_array($result)) {
        _menu_delete_item($link);
      }
    }
    drupal_set_message(t('SharePoint CMIS settings have been saved.'));
  }
  catch(Exception$e) {
    drupal_set_message(t('Could not save SharePoint CMIS settings.'), 'error');
  }
}

