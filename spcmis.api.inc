<?php
// $Id$


/**
 * @file
 * CMIS 0.5 client implementation
 *
 * @todo
 *   Document all CMIS calls
 *   Update methods signatures to match CMIS specs
 */

/*
 * Repository services
 */
function spcmisapi_getRepositories($endpoint_service) {
  return spcmis_vendor_invoke('getRepositories', $endpoint_service);
}

function spcmisapi_getRepositoryInfo() {
  return spcmis_vendor_invoke('getRepositoryInfo');
}

function spcmisapi_getTypes($repositoryId, $typeId = NULL) {
  return spcmis_vendor_invoke('getTypes', $repositoryId, $typeId);
}

function spcmisapi_getTypeDefinition($repositoryId, $typeId, $options = array()) {
  return spcmis_vendor_invoke('getTypeDefinition', $repositoryId, $typeId, $options);
}

function spcmisapi_getObjectTypeDefinition($repositoryId, $objectId) {
  return spcmis_vendor_invoke('getObjectTypeDefinition', $repositoryId, $objectId);
}

/*
 * Navigation servicies
 */
function spcmisapi_getFolderTree($repositoryId) {
  return spcmis_vendor_invoke('getFolderTree', $repositoryId);
}

function spcmisapi_getDescendants($repositoryId, $folderId) {
  return spcmis_vendor_invoke('getDescendants', $repositoryId, $folderId);
}

function spcmisapi_getChildren($repositoryId, $objectId, $options = array()) {
  return spcmis_vendor_invoke('getChildren', $repositoryId, $objectId, $options);
}

function spcmisapi_getFolderParent($repositoryId, $objectId, $options = array()) {
  return spcmis_vendor_invoke('getFolderParent', $repositoryId, $objectId, $options);
}

function spcmisapi_getObjectParents($repositoryId, $objectId, $options = array()) {
  return spcmis_vendor_invoke('getObjectParents', $repositoryId, $objectId, $options);
}

function spcmisapi_getCheckedOutDocs($repositoryId, $options = array()) {
  return spcmis_vendor_invoke('getCheckedOutDocs', $repositoryId, $options);
}

/*
 * Object services
 */
function spcmisapi_getObject($repositoryId, $objectId, $options = array()) {
  return spcmis_vendor_invoke('getObjectByPath', $repositoryId, $objectId, $options);
}

function spcmisapi_getObjectByPath($repositoryId, $path, $options = array()) {
  return spcmis_vendor_invoke('getObjectByPath', $repositoryId, $path, $options);
}

function spcmisapi_createDocument($repositoryId, $folderId, $fileName, $properties = array(), $content = NULL, $contentType = "application/octet-stream", $options = array()) {
  return spcmis_vendor_invoke('createDocument', $repositoryId, $folderId, $fileName, $properties, $content, $contentType, $options);
}

function spcmisapi_createFolder($repositoryId, $folderId, $folderName, $properties = array(), $options = array()) {
  return spcmis_vendor_invoke('createFolder', $repositoryId, $folderId, $folderName, $properties, $options);
}

function spcmisapi_createRelationship($repositoryId, $typeId, $properties, $sourceObjectId, $targetObjectId) {
  return spcmis_vendor_invoke('createRelationship', $repositoryId, $typeId, $properties, $sourceObjectId, $targetObjectId);
}

function spcmisapi_createPolicy($repositoryId, $typeId, $properties, $folderId) {
  return spcmis_vendor_invoke('createPolicy', $repositoryId, $typeId, $properties, $folderId);
}

function spcmisapi_getAllowableActions($repositoryId, $objectId, $options = array()) {
  return spcmis_vendor_invoke('getAllowableActions', $repositoryId, $objectId, $options);
}

function spcmisapi_getRenditions($repositoryId, $objectId, $options = array('renditionFilter' => "*")) {
  return spcmis_vendor_invoke('getProperties', $repositoryId, $objectId, $options);
}

function spcmisapi_getProperties($repositoryId, $objectId, $options = array()) {
  return spcmis_vendor_invoke('getProperties', $repositoryId, $objectId, $options);
}

function spcmisapi_getContentStream($repositoryId, $objectId, $options = array()) {
  return spcmis_vendor_invoke('getContentStream', $repositoryId, $objectId, $options);
}

function spcmisapi_updateProperties($repositoryId, $objectId, $properties = array(), $options = array()) {
  return spcmis_vendor_invoke('updateProperties', $repositoryId, $objectId, $properties, $options);
}

function spcmisapi_moveObject($repositoryId, $objectId, $targetFolderId, $sourceFolderId = NULL, $options = array()) {
  return spcmis_vendor_invoke('moveObject', $repositoryId, $objectId, $targetFolderId, $sourceFolderId, $options);
}

function spcmisapi_deleteObject($repositoryId, $objectId, $options = array()) {
  return spcmis_vendor_invoke('deleteObject', $repositoryId, $objectId, $options);
}

function spcmisapi_deteleTree($repositoryId, $folderId, $unfileNonfolderObjects) {
  return spcmis_vendor_invoke('deleteTree', $repositoryId, $folderId, $unfileNonfolderObjects);
}

function spcmisapi_setContentStream($repositoryId, $objectId, $content = NULL, $contentType = NULL, $options = array()) {
  return spcmis_vendor_invoke('setContentStream', $repositoryId, $objectId, $content, $contentType, $options);
}

function spcmisapi_deleteContentStream($repositoryId, $objectId, $options = array()) {
  return spcmis_vendor_invoke('deleteContentStream', $repositoryId, $objectId, $options);
}

/*
 * Multi-filling services
 */
function spcmisapi_addObjectToFolder($repositoryId, $objectId, $folderId) {
  return spcmis_vendor_invoke('addObjectToFolder', $repositoryId, $objectId, $folderId);
}

function spcmisapi_removeObjectFromFolder($repositoryId, $objectId, $folderId = NULL) {
  return spcmis_vendor_invoke('removeObjectFromFolder', $repositoryId, $objectId, $folderId);
}

/*
 * Versioning services
 */
function spcmisapi_checkOut($repositoryId, $documentId) {
  return spcmis_vendor_invoke('checkOut', $repositoryId, $objectId);
}

function spcmisapi_cancelCheckOut($repositoryId, $documentId) {
  return spcmis_vendor_invoke('cancelCheckOut', $repositoryId, $objectId);
}

function spcmisapi_checkIn($repositoryId, $documentId, $major = NULL, $bag = NULL, $content = NULL, $checkinComment = NULL) {
  return spcmis_vendor_invoke('checkIn', $repositoryId, $documentId, $major, $bag, $content, $checkinComment);
}

function spcmisapi_getPropertiesOfLatestVersion($repositoryId, $versionSeriesId) {
  return spcmis_vendor_invoke('getPropertiesOfLatestVersion', $repositoryId, $versionSeriesId);
}

function spcmisapi_getAllVersions($repositoryId, $versionSeriesId) {
  return spcmis_vendor_invoke('getAllVersions', $repositoryId, $versionSeriesId);
}

function spcmisapi_deleteAllVersions($repositoryId, $versionSeriesId) {
  return spcmis_vendor_invoke('deleteAllVersions', $repositoryId, $versionSeriesId);
}

/*
 * Relationships services
 */
function spcmisapi_getRelationships($repositoryId, $objectId) {
  return spcmis_vendor_invoke('getRelationships', $repositoryId, $objectId);
}

/*
 * Policy services
 */
function spcmisapi_applyPolicy($repositoryId, $policyId, $objectId) {
  return spcmis_vendor_invoke('applyPolicy', $repositoryId, $policyId, $objectId);
}

function spcmisapi_removePolicy($repositoryId, $policyId, $objectId) {
  return spcmis_vendor_invoke('removePolicy', $repositoryId, $policyId, $objectId);
}

function spcmisapi_getAppliedPolicies($repositoryId, $objectId) {
  return spcmis_vendor_invoke('getAppliedPolicies', $repositoryId, $objectId);
}

