<?php
// $Id$


/**
 * Build cmis_browser browse page.
 *
 */
function _spcmis_search_spcmis_drupal_update() {
  module_load_include('api.inc', 'spcmis');
  module_load_include('inc', 'node', 'node.pages');

  try {
    //Get the list of libraries from db
    $cmis_repositories = variable_get('cmis_repositories');
    $arr_all_doc = array();
    if (isset($cmis_repositories)) {
      foreach ($cmis_repositories as $gid => $library) {
        if ($library['repository_enable'] == 1) {
          _spcmis_search_parse_metadata($gid, '-1', &$arr_all_doc);
        }
      }
      _spcmis_search_delete_metadata($arr_all_doc);
    }
  }
  catch(Exception$exc) {
    echo $exc->getTraceAsString();
  }
}

/*
 * Parse cmis metadata and save it in the database
 * This function is called recursively to handle files in subfolders
*/
function _spcmis_search_parse_metadata($gid, $objectId, &$arr_all_doc) {

  $doc_metadata = array();

  $repo_object = spcmisapi_getProperties($gid, $objectId);
  $children = spcmisapi_getChildren($gid, $repo_object->id)->objectList;
  foreach ($children as $child) {
    switch ($child->properties['cmis:baseTypeId']) {
      case 'cmis:folder':
        //if type is folder make a recursive call so the folder contents are parsed
        _spcmis_search_parse_metadata($gid, $child->id, &$arr_all_doc);
        break;

      case 'cmis:document':
        //if type is document save the metadata
        $doc_metadata['id'] = $child->id;
        $doc_metadata['name'] = $child->properties['cmis:name'];
        $doc_metadata['created_by'] = $child->properties['cmis:createdBy'];
        $doc_metadata['creation_date'] = $child->properties['cmis:creationDate'];
        $doc_metadata['last_modified_by'] = $child->properties['cmis:lastModifiedBy'];
        $doc_metadata['last_modification_date'] = $child->properties['cmis:lastModificationDate'];
        $doc_metadata['group_id'] = $gid;
        if ($gid != 'default') {
          $doc_metadata['download_url'] = $GLOBALS['base_url'] . '?q=node/' . $gid . '/spcmis/browser&id=' . $child->id;
        }
        else $doc_metadata['download_url'] = $GLOBALS['base_url'] . '?q=spcmis/browser&id=' . $child->id;

        $doc_metadata['file_type'] = $child->properties['cmis:contentStreamMimeType'];

        $arr_all_doc[] = $child->id;

        _spcmis_search_save_metadata($doc_metadata);
        break;
    }
  }
}

/*
 * Save metadata as a node
*/
function _spcmis_search_save_metadata($doc_metadata) {

  $sql_query    = "SELECT nid, field_last_modification_date_value FROM {content_type_librarydocument} WHERE field_id_value = '%s' AND field_group_id_value = '%s'";
  $query_result = db_query($sql_query, array($doc_metadata['id'], $doc_metadata['group_id']));
  $obj          = db_fetch_object($query_result);

  if ($obj->nid) if (strtotime($obj->field_last_modification_date_value) != strtotime($doc_metadata['last_modification_date'])) {
    // If node is already present, update it
    $node = node_load($obj->nid);
  }
  else return;
  // If node is not already present create a new one
  else
  $node = new stdClass();

  $node->type = 'librarydocument';
  $node->uid = 1;
  $node->status = 1;
  $node->title = $doc_metadata['name'];
  $node->field_id[0]['value'] = $doc_metadata['id'];
  $node->field_created_by[0]['value'] = $doc_metadata['created_by'];
  $node->field_creation_date[0]['value'] = $doc_metadata['creation_date'];
  $node->field_last_modified_by[0]['value'] = $doc_metadata['last_modified_by'];
  $node->field_last_modification_date[0]['value'] = $doc_metadata['last_modification_date'];
  $node->field_group_id[0]['value'] = $doc_metadata['group_id'];
  $node->field_download_url[0]['url'] = $doc_metadata['download_url'];
  $node->field_file_type[0]['value'] = $doc_metadata['file_type'];

  //Map Group specific nodes to the OG gids so they're viewable only by group members
  if ($doc_metadata['group_id'] != 'default') {
    $node->og_initial_groups[0][$doc_metadata['group_id']];
    $node->og_groups[0] = $doc_metadata['group_id'];
    $node->og_public = 0;
  }

  node_save($node);
}

//Delete metadata nodes that are no longer relevant
//i.e. deleted from SharePoint
function _spcmis_search_delete_metadata($arr_all_docs_id) {
  $sql_query = "SELECT nid, field_id_value FROM {content_type_librarydocument}";
  $query_result = db_query($sql_query);

  while ($row = db_fetch_object($query_result)) {
    if (!in_array($row->field_id_value, $arr_all_docs_id)) {
      node_delete($row->nid);
    }
  }
}

