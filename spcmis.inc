<?php
// $Id$


/**
 * Get a spcmis client pointing to a particular spcmis web service.
 *
 * @param string $service the web service.
 *
 * @return SoapClient a SoapClient ready for use.
 * We need to use SOAP to get all list collections because CMIS
 * for SharePoint doesn't do this at root level.
 */
include ("nusoap/nusoap.php");
function spcmis_get_client($endpoint, $service = 'lists') {
  $soapclient = new nusoap_client($endpoint['endpoint']['url'] . '/_vti_bin/' . $service . '.asmx?WSDL', 'false');
  $soapclient->setDebugLevel(9);
  $soapclient->setCredentials($endpoint['endpoint']['user'], $endpoint['endpoint']['password'], 'ntlm');
  return $soapclient;
}

/**
 * Call a particulare SOAP request in on a web service
 *
 * @param string $service the web service.
 *
 * @return Array containing the result
 */
function spcmis_call($client, $call, $arguments = array()) {
  $result = $client->call($call, $arguments);
  if ($client->getError()) {
    if (variable_get('sharepoint_debug', 0)) {
      drupal_set_message($client->getError(), 'error');
      drupal_set_message($client->getDebug(), 'error');
    }
  }
  return $result;
}

//Get a list of all repositories in SharePoint
function spcmis_get_splist_collection($client, $show_hidden = FALSE) {
  $result = spcmis_call($client, 'GetListCollection');
  return $result['GetListCollectionResult']['Lists']['List'];
}

