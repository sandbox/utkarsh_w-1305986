<?php
// $Id$


/**
 * Menu callback -- ask for confirmation of CMIS object deletion
 *
 */
function spcmis_browser_content_delete_confirm(&$form_state) {
  module_load_include('api.inc', 'spcmis');
  module_load_include('utils.inc', 'spcmis_browser');

  $cmis_objectId = urldecode($_GET['id']);
  $form['cmis_objectId'] = array('#type' => 'value', '#value' => $cmis_objectId);
  $form['return_url'] = array('#type' => 'value', '#value' => $_GET['return_url']);

  try {
    $nid         = get_group_id_from_url();
    $repository  = spcmis_get_repository($nid);
    $cmis_object = spcmisapi_getProperties($repository->repositoryId, $cmis_objectId);
  }
  catch(CMISException$e) {
    spcmis_error_handler('spcmis_content_delete', $e);
  }

  return confirm_form($form,
    t('Are you sure you want to delete %name?', array('%name' => $cmis_object->properties['cmis:name'])),
    $_GET['return_url'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Execute CMIS object deletion.
 *
 */
function spcmis_browser_content_delete_confirm_submit($form, &$form_state) {
  module_load_include('api.inc', 'spcmis');

  if ($form_state['values']['confirm']) {
    $cmis_objectId = $form_state['values']['cmis_objectId'];
    try {
      //NITOR: Check if the call is made from within a group or the shared library
      $nid         = get_group_id_from_url();
      $repository  = spcmis_get_repository($nid);
      $cmis_object = spcmisapi_getProperties($repository->repositoryId, $cmis_objectId);
      $content     = spcmisapi_deleteObject($repository->repositoryId, $cmis_object->id);
      drupal_set_message(t('CMIS object @name has been deleted.', array('@name' => $cmis_object->properties['cmis:name'])));
    }
    catch(CMISException$e) {
      spcmis_error_handler('spcmis_content_delete', $e);
    }
  }

  $form_state['redirect'] = $form_state['values']['return_url'];
}

