<?php
// $Id$


/**
 * Build cmis_browser browse page.
 *
 */
function spcmis_browser_content_get() {

  module_load_include('api.inc', 'spcmis');
  module_load_include('utils.inc', 'spcmis_browser');
  // Nitor fetch group id from url
  $nid = get_group_id_from_url();
  // Invoke CMIS service
  try {
    $repository = spcmis_get_repository($nid);
    $object = _spcmis_browser_content_object_from_request($repository);

    switch ($object->properties['cmis:baseTypeId']) {
      case 'cmis:document':
        return _spcmis_browser_content_get_document($repository, $object);

      case 'cmis:folder':
        //return _cmis_browser_content_get_folder($repository, $object, array_slice(explode('/', $_GET['q']), 2));
        //Nitor check if call is from group or main site library
        // Nitor previously 2
        if (isset($nid))
        return _spcmis_browser_content_get_folder($repository, $object, array_slice(explode('/', $_GET['q']), 4));
      else return _spcmis_browser_content_get_folder($repository, $object, array_slice(explode('/', $_GET['q']), 2));
      //
      break;

      default:
        throw new CMISException(t('Unable to handle cmis object @object_id of type @object_type', array(
              '@object_id' => $object->id,
              '@object_type' => $object->type,
            )));
    }
  }
  catch(CMISException$e) {
    spcmis_error_handler('cmis_browser', $e);
    return '';
  }
}

/**
 * CMIS document download handler.
 *
 */
function _spcmis_browser_content_get_document($repository, $object) {
  module_load_include('api.inc', 'spcmis');

  try {
    $content = spcmisapi_getContentStream($repository->repositoryId, $object->id);
  }
  catch(CMISException$e) {
    spcmis_error_handler('spcmis_browser_content_get', $e);
    drupal_set_header('HTTP/1.1 503 Service unavailable');
    exit();
  }

  if (ob_get_level()) {
    ob_end_clean();
  }

  drupal_set_header('Cache-Control: no-cache, must-revalidate');
  drupal_set_header('Content-type: ' . $object->properties['cmis:contentStreamMimeType']);
  drupal_set_header('Content-Disposition: attachment; filename="' . $object->properties['cmis:name'] . '"');

  //NITOR: Added specifically for SP2010 as it returns the content encoded as base64
  if (base64_decode($content, TRUE)) {
    $content = base64_decode($content);
  }

  print ($content);

  exit();
}

/**
 * CMIS folder browser handler.
 *
 */
function _spcmis_browser_content_get_folder($repository, $object) {
  try {
    $children = spcmisapi_getChildren($repository->repositoryId, $object->id)->objectList;
  }
  catch(CMISException$e) {
    spcmis_error_handler('spcmis_browser', $e);
    return '';
  }

  return theme('spcmis_browser', array(
      'children' => $children,
      'bcarray' => explode('/', substr($object->properties['cmis:path'], 1)),
    ));
}

