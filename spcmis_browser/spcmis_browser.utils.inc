<?php
// $Id$


/**
 * Helper method used to lookup CMIS object from request.
 *
 */
function _spcmis_browser_content_object_from_request($repository) {
  $object_id = NULL;
  $object_path = NULL;

  //$bcarray = array_slice(explode('/', $_GET['q']), 2);
  //Nitor fetch group id from url
  $nid = get_group_id_from_url();
  if (isset($nid)) {
    $bcarray = array_slice(explode('/', $_GET['q']), 4);
  }
  else $bcarray = array_slice(explode('/', $_GET['q']), 2);
  //
  if (array_key_exists('id', $_GET)) {
    // grab objectId from GET
    $object_id = urldecode($_GET['id']);
  }
  elseif (!empty($bcarray)) {
    // grab path
    $object_path = drupal_urlencode('/' . implode('/', $bcarray));
  }
  elseif (array_key_exists('browser_default_folderId', $repository->settings)) {
    // grab default folderId from repository's settings
    $object_id = $repository->settings['browser_default_folderId'];
  }
  elseif (array_key_exists('browser_default_folderPath', $repository->settings)) {
    // grab default folderPath from repository's settings
    $object_path = drupal_urlencode($repository->settings['browser_default_folderPath']);
  }
  else {
    // fallback to repository's root folderId
    //NITOR: Handle "cmis:" for SP 2010
    if (isset($repository->info->repositoryInfo['cmis:rootFolderId'])) {
      $object_id = $repository->info->repositoryInfo['cmis:rootFolderId'];
    }
    else $object_id = $repository->info->repositoryInfo['rootFolderId'];
  }

  if (!is_null($object_id)) {
    $object = spcmisapi_getProperties($repository->repositoryId, $object_id);
  }
  elseif (!is_null($object_path)) {
    $object = spcmisapi_getObjectByPath($repository->repositoryId, $object_path);
  }
  else {
    throw new CMISException('Unknown CMIS object');
  }

  return $object;
}

function get_group_id_from_url() {
  //Nitor get the node id if present
  $get_q_array = explode('/', $_GET['q']);
  if ($get_q_array[0] == 'node') {
    $nid = $get_q_array[1];
  }
  else $nid = NULL;
  return $nid;
  //
}

//Returns icon for specific mime types
function spcmis_browser_get_file_icon($mimeType) {
  switch ($mimeType) {
    case 'application/pdf':
      $icon = theme('image', drupal_get_path('module', 'spcmis_browser') . '/images/pdf-icon.gif');
      break;

    case 'application/vnd.ms-word.document.12':
    case 'application/msword':
      $icon = theme('image', drupal_get_path('module', 'spcmis_browser') . '/images/icdocx.png');
      break;

    case 'image/jpg':
    case 'image/jpeg':
      $icon = theme('image', drupal_get_path('module', 'spcmis_browser') . '/images/image-jpeg.gif');
      break;

    case 'image/bmp':
    case 'image/gif':
    case 'image/png':
      $icon = theme('image', drupal_get_path('module', 'spcmis_browser') . '/images/image.gif');
      break;

    case 'application/vnd.ms-powerpoint.presentation.12':
    case 'application/vnd.ms-powerpoint':
      $icon = theme('image', drupal_get_path('module', 'spcmis_browser') . '/images/icpptx.png');
      break;

    case 'application/vnd.ms-excel.12':
    case 'application/vnd.ms-excel':
      $icon = theme('image', drupal_get_path('module', 'spcmis_browser') . '/images/icxls.png');
      break;

    case 'text/html':
    case 'text/htm':
      $icon = theme('image', drupal_get_path('module', 'spcmis_browser') . '/images/ichtm.gif');
      break;

    case 'text/xml':
      $icon = theme('image', drupal_get_path('module', 'spcmis_browser') . '/images/xml.gif');
      break;

    case 'text/css':
      $icon = theme('image', drupal_get_path('module', 'spcmis_browser') . '/images/iccss.gif');
      break;

    default:
      $icon = theme('image', drupal_get_path('module', 'spcmis_browser') . '/images/ictxt.gif');
  }

  return $icon;
}

