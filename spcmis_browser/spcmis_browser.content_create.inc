<?php
// $Id$


/**
 * Create $folder_name under $parent_folder
 *
 */
function _spcmis_browser_actions_folder_create($parent_folder_path, $folder_name) {
  module_load_include('api.inc', 'spcmis');

  $repository = spcmis_get_repository();
  try {
    $parent_folder = spcmisapi_getObjectByPath($repository->repositoryId, $parent_folder_path);
  }
  catch(CMISException$e) {
    spcmis_error_handler('_spcmis_browser_actions_folder_create', $e);
    form_set_error('path', t('Error while trying to lookup @object_id', array('@path' => $parent_folder_path)));
    return '';
  }

  if ($parent_folder->properties['cmis:baseTypeId'] == 'cmis:folder') {
    try {
      $folder_id = spcmisapi_createFolder($repository->repositoryId, $parent_folder->id, $folder_name);
      drupal_set_message(t('CMIS object @name has been created.', array('@name' => $folder_name)));
    }
    catch(CMISException$e) {
      spcmis_error_handler('spcmis_folder_create_form_submit', $e);
      form_set_error('path', t('Unable to create @object_name object.', array('@object_name' => $folder_name)));
      return '';
    }
  }
  else {
    form_set_error('path', t('Error while trying to lookup @object_path', array('@object_path' => $parent_folder_path)));
    return FALSE;
  }
}

/**
 * Create $content_name under $parent_folder folder.
 *
 */
function _spcmis_browser_actions_content_create($folder_path, $document_name, $document_content, $document_mimetype = 'text/html') {
  module_load_include('api.inc', 'spcmis');

  try {
    $repository = spcmis_get_repository();
    $folder = spcmisapi_getObjectByPath($repository->repositoryId, $folder_path);
  }
  catch(CMISException$e) {
    spcmis_error_handler('_spcmis_browser_actions_content_create', $e);
    form_set_error('path', t('Error while trying to lookup @object_path', array('@object_path' => $folder_path)));
    return;
  }

  if ($folder->properties['cmis:baseTypeId'] == 'cmis:folder') {
    try {
      $document_id = spcmisapi_createDocument($repository->repositoryId, $folder->id, $document_name, array(), $document_content, $document_mimetype);
      drupal_set_message(t('CMIS object @object_name has been created.', array('@object_name' => $document_name)));
    }
    catch(CMISException$e) {
      spcmis_error_handler('_spcmis_browser_actions_content_create', $e);
      form_set_error('path', t('Unable to create @object_name object.', array('@object_name' => $document_name)));
      return '';
    }
  }
  else {
    form_set_error('path', t('Error while locating the target space @object_id', array('@object_id' => $folder_path)));
    return FALSE;
  }
}

