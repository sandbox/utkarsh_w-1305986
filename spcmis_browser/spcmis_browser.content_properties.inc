<?php
// $Id$


/**
 * CMIS object properties page.
 *
 */
function spcmis_browser_content_properties() {
  module_load_include('api.inc', 'spcmis');
  module_load_include('utils.inc', 'spcmis_browser');

  // Invoke CMIS service
  try {
    //Nitor fetch group id from url and pass it to spcmis_get_repository
    $nid        = get_group_id_from_url();
    $repository = spcmis_get_repository($nid);
    $object     = _spcmis_browser_content_object_from_request($repository);

    $output = theme('spcmis_browser_content_properties', $object);

    if (isset($_GET['no_layout'])) {
      print $output;
      exit();
    }

    return $output;
  }
  catch(CMISException$e) {
    spcmis_error_handler('spcmis_browser', $e);
    return '';
  }
}

